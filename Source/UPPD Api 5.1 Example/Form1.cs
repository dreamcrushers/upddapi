//Prepared by Simon - Phasor Design
//to demo. TBAPI in C# 5th November 2010

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using Yipkos.UPDDApi;
using Yipkos.UPDDApi.Structs;

namespace _50101
{
    public partial class Form1 : Form
    {
        uint _lastTick;

        public Dictionary<int, Monitor> _monitors { get; set; }
        public Dictionary<int, Dictionary<int, TouchPoint>> _touches { get; set; }
        public MyCallback my_delegate;

        public Form1()
        {
            InitializeComponent();    
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Beep(400, 1000);

            StartTBApi();

            button1.Enabled = false;
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StopTBApi();

            Beep(660, 500);

            button2.Enabled = false;
            button1.Enabled = true;
        }

        private unsafe void call_function(int context, TBApiData* data)
        {
            Invoke((MethodInvoker)delegate
            {
                UpdateFormDataOnSameThreadAsForm(context, data);
            });
        }

        private unsafe void UpdateFormDataOnSameThreadAsForm(int context, TBApiData* data)
        {
            try
            {
                bool reportIt = false;

                if (data->Type == 1)
                { // this is an xy event
                    var touchPoint = new TouchPoint
                    {
                        X = ((int)data->CalX * (_monitors[data->Device].Width - 1) / 65535) + _monitors[data->Device].Left,
                        Y = ((int)data->CalY * (_monitors[data->Device].Height - 1) / 65535) + _monitors[data->Device].Top
                    };

                    if (!_touches[data->Device].ContainsKey(data->Touch))
                    { // this is initial movement for a device / contact pair add to dictionary
                        _touches[data->Device].Add(data->Touch, touchPoint);
                        reportIt = true;
                    }
                    else
                    { // update the position
                        _touches[data->Device][data->Touch] = touchPoint;
                        if ((data->Tick - _lastTick) > 20)
                        { // limit the number of position changes we report
                            _lastTick = data->Tick;
                            reportIt = true;
                        }
                    }
                }
                if (data->Type == 0x40000000)
                {
                    TouchEvent* p_event = (TouchEvent*)data;
                    if (p_event->State == 0)
                    { // this is a "liftoff" event, remove the contact from the dictionary
                        _touches[data->Device].Remove(data->Touch);

                        reportIt = true;
                    }
                }
                if (reportIt)
                {
                    textBox1.Text = "";
                    foreach (var device in _touches)
                    {
                        foreach (var touch in device.Value)
                        {
                            textBox1.Text += "Device ID: " + device.Key.ToString() + " Touch ID: " + touch.Key.ToString() + " X: " + touch.Value.X.ToString() + " Y: " + touch.Value.Y.ToString() + "\r\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void StartTBApi()
        {
            InitializeProperties();

            InitializeApi();

            OpenApiConnection();

            RegisterTouchMonitors();

            DisableMousePortInterface();

            SetupCallBack();

            RegisterCallBack();
        }

        private void StopTBApi()
        {
            UnregisterCallBack();

            DisableMousePortInterface();

            CloseApiConnection();

            Terminate();
        }

        private void InitializeProperties()
        {
            _lastTick = 0;
            _monitors = new Dictionary<int, Yipkos.UPDDApi.Structs.Monitor>();
            _touches = new Dictionary<int, Dictionary<int, Yipkos.UPDDApi.Structs.TouchPoint>>();
        }

        private bool DeviceExist(int deviceHandle)
        {
            try
            {
                var deviceExists = TBApi.DLL_TBApiValidateDevice(deviceHandle);
                if (!deviceExists)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void Beep(int frequency, int duration)
        {
            try
            {
                TBApi.Beep(frequency, duration);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void InitializeApi()
        {
            try
            {
                TBApi.DLL_TBApiInitEx(0);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void OpenApiConnection()
        {
            try
            {
                int result;
                result = TBApi.DLL_TBApiOpen();
                if (result == 0)
                {
                    MessageBox.Show("Bad Open");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void RegisterTouchMonitors()
        {
            try
            {
                var deviceHandle = TBApi.DLL_TBApiGetRelativeDevice(0);

                for (int i = 0; deviceHandle != 0;)
                {
                    _touches[deviceHandle] = new Dictionary<int, Yipkos.UPDDApi.Structs.TouchPoint>();

                    Yipkos.UPDDApi.Structs.Monitor m;
                    m.PrimaryWidth = 0;
                    m.PrimaryHeight = 0;
                    m.Width = 0;
                    m.Height = 0;
                    m.Left = 0;
                    m.Top = 0;

                    TBApi.DLL_TBApiGetMonitorMetrics(deviceHandle, ref m.PrimaryWidth, ref m.PrimaryHeight, ref m.Width, ref m.Height, ref m.Left, ref m.Top);
                    _monitors.Add(deviceHandle, m);

                    deviceHandle = TBApi.DLL_TBApiGetRelativeDevice(++i);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void DisableMousePortInterface()
        {
            try
            {
                TBApi.DLL_TBApiMousePortInterfaceEnable(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void SetupCallBack()
        {
            unsafe
            {
                try
                {
                    my_delegate = this.call_function;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return;
                }
            }

        }

        private void RegisterCallBack()
        {
            try
            {
                int result;
                result = TBApi.DLL_TBApiRegisterDataCallback(0, 0, 0x40000001, my_delegate);
                if (result == 0)
                {
                    MessageBox.Show("Bad Reg.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }

        }

        private bool IsApiActive()
        {
            try
            {
                int result = TBApi.DLL_TBApiRegisterDataCallback(0, 0, 3, my_delegate);
                if (result == 0)
                {
                    MessageBox.Show("Bad Reg.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void UnregisterCallBack()
        {
            try
            {
                int result = TBApi.DLL_TBApiUnregisterDataCallback(my_delegate);
                if (result == 0)
                {
                    MessageBox.Show("Bad UnReg.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private static void EnableMousePortInterface()
        {
            try
            {
                TBApi.DLL_TBApiMousePortInterfaceEnable(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private static void CloseApiConnection()
        {
            try
            {
                TBApi.DLL_TBApiClose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        private void Terminate()
        {
            try
            {
                TBApi.DLL_TBApiTerminate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

    }
}