﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yipkos.UPDDApi.Structs
{
    public struct TouchPoint
    {
        public int X;
        public int Y;
    }
}
