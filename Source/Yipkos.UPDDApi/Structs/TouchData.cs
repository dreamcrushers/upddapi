﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yipkos.UPDDApi.Structs
{
    public unsafe struct TBApiData
    {
        public uint Tick;
        public uint RawX;
        public uint RawY;
        public uint CalX;
        public uint CalY;
        public fixed byte DataInside[64 - (4 * 4)];   //to make up correct size of 64 bytes.
        public byte Device;
        public byte Touch;
        public uint Type;
        public uint Unused;
    };
}
