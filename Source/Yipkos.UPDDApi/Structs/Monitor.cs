﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yipkos.UPDDApi.Structs
{
    public struct Monitor
    {
        public int PrimaryWidth;
        public int PrimaryHeight;
        public int Width;
        public int Height;
        public int Left;
        public int Top;
    };
}
