﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yipkos.UPDDApi.Structs
{
    public unsafe struct TouchEvent
    {
        public uint Tick;
        public byte State;
        public byte Time;
        public byte IsToolbarTouch;
        public fixed byte DataInside[64 - 3];   //to make up correct size of 80 bytes.
        public byte Device;
        public byte Stylus;
        public uint Type;
        public uint Unused;
    };
}
