﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Yipkos.UPDDApi.Structs;

namespace Yipkos.UPDDApi
{
    unsafe public delegate void MyCallback(int context, TBApiData* p_mydata);

    public class TBApi
    {
        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiInit(int i);

        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiInitEx(int i);

        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiInitEx(string path);

        [DllImport("TBApi64.dll")]
        public static extern int DLL_TBApiOpen();

        [DllImport("TBApi64.dll")]
        public static extern int DLL_TBApiGetRelativeDevice(int position);

        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiMousePortInterfaceEnable(int state);

        [DllImport("TBApi64.dll")]
        public static extern int DLL_TBApiRegisterDataCallback(
                    int aDeviceHandle,
                    int aContext, int aTypes,
                    MyCallback callback
                    );

        [DllImport("TBApi64.dll")]
        public static extern int DLL_TBApiUnregisterDataCallback(MyCallback callback);

        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiTerminate();

        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiClose();

        [DllImport("TBApi64.dll")]
        public static extern bool DLL_TBApiIsApiActive();

        [DllImport("TBApi64.dll")]
        public static extern bool DLL_TBApiValidateDevice(int deviceHandle);

        [DllImport("TBApi64.dll")]
        unsafe public static extern void DLL_TBApiGetMonitorMetrics(int aDevice,
                                  ref Int32 aPrimaryMonitorWidth, ref Int32 aPrimaryMonitorHeight,
                                  ref Int32 aMonitorWidth, ref Int32 aMonitorHeight, ref Int32 aMonitorLeft, ref Int32 aMonitorTop);

        [DllImport("kernel32.dll")]
        public static extern void Beep(int freq, int dur);
    }
}
