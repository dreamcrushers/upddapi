﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Yipkos.UPDDApi;

namespace DesktopExample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnInitTb_Click(object sender, EventArgs e)
        {


            try
            {
                TBApi.DLL_TBApiInit(0);
            }
            catch(Exception ex)
            {
                throw;
            }

            try
            {
                int result;
                result = TBApi.DLL_TBApiOpen();
                if (result == 0)
                {
                    MessageBox.Show("Bad Open");
                    return;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return;
            }

            try
            {
                int result;
                result = TBApi.DLL_TBApiGetRelativeDevice(0);

                //MessageBox.Show("Handle: " + result.ToString());

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return;
            }

            try
            {
                TBApi.DLL_TBApiMousePortInterfaceEnable(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
            //unsafe
            //{
            //    try
            //    {
            //        my_delegate = this.call_function;
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.ToString());
            //        return;
            //    }
            //}


            //try
            //{
            //    int result;
            //    result = TBApi.DLL_TBApiRegisterDataCallback(0, 0, 3, my_delegate);
            //    if (result == 0)
            //    {
            //        MessageBox.Show("Bad Reg.");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //    return;
            //}


        }
    }
}
