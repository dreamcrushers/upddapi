//Prepared by Simon - Phasor Design
//to demo. TBAPI in C# 5th November 2010

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;



namespace _50101
{


    public partial class Form1 : Form
    {
        int counter;
        uint last_tick;

        //It seems to be necessary to re-define the the data structure
        //equivalent of union is required.

        public unsafe struct data_block
        {
            public  uint tick;
            public  uint rawx;
            public  uint rawy;
            public  uint calx;
            public  uint caly;
            public fixed byte  data_inside[76 - (4 * 6)- (2 * 1)];   //to make up correct size of 76 bytes.
            public byte hDevice;
            public byte hStylus;
            public uint type;
        } ;

        public data_block my_data_block;

        public unsafe struct event_block
        {
            public uint tick;
            public  byte left;
            public byte right;
            public byte timed;
            public fixed byte  data_inside[76 - (4 * 2) - (5 * 1)];   //to make up correct size of 76 bytes.
            public byte hDevice;
            public byte hStylus;
            public uint type;
        } ;

        public event_block my_event_block;



        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiInit(int i);

        [DllImport("TBApi64.dll")]
        public static extern int DLL_TBApiOpen();

        [DllImport("TBApi64.dll")]
        public static extern int DLL_TBApiGetRelativeDevice(int position);

        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiMousePortInterfaceEnable(int state);

        unsafe public delegate void MyCallback(int context, data_block* p_mydata);

        [DllImport("TBApi64.dll")]
        public static extern int DLL_TBApiRegisterDataCallback(
                    int aDeviceHandle, 
                    int aContext, int aTypes,
                    MyCallback callback
                    );


        [DllImport("TBApi64.dll")]
        public static extern int DLL_TBApiUnregisterDataCallback(
                    
                    MyCallback callback
                    );


        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiTerminate ();

        [DllImport("TBApi64.dll")]
        public static extern void DLL_TBApiClose();


        [DllImport("kernel32.dll")]
        public static extern void Beep(int freq, int dur);


        public Form1()
        {
            InitializeComponent();
            last_tick = 0;
            
        }

        unsafe public void call_function(int context, data_block * data)
        {
            try
            {

                counter++;
                //this.Text = "Count: " + counter.ToString();
                //this.Text += "Context: " + context.ToString();

                //MessageBox.Show(context.ToString());

                if (data->type == 1)
                {

                    my_data_block.tick = data->tick;
                    my_data_block.rawx = data->rawx;
                    my_data_block.rawy = data->rawy;
                    my_data_block.calx = data->calx;
                    my_data_block.caly = data->caly;
                    my_data_block.hDevice = data->hDevice;
                    my_data_block.hStylus = data->hStylus;
                    my_data_block.type = data->type;
                }
                if (data->type == 2)
                {
                    event_block * p_event = (event_block *) data;
                    my_event_block.tick = p_event->tick;
                    my_event_block.left = p_event->left;
                    my_event_block.right = p_event->right;
                    my_event_block.timed = p_event->timed;
                    my_event_block.hDevice = p_event->hDevice;
                    my_event_block.hStylus = p_event->hStylus;
                    my_event_block.type = p_event->type;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }

            try
            {
                this.Invoke(
                    (MethodInvoker)delegate 
                    {
                        if (data->type == 1) UpDate_Type_1();
                        if (data->type == 2) UpDate_Type_2();
                        
                        this.Text = "Count: " + counter.ToString();
                        this.Text += " Context: " + context.ToString();
                        this.Text += " Type: " + data->type.ToString();




                    }
                );
                

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }




        }

        MyCallback my_delegate;


        private void button1_Click(object sender, EventArgs e)
        {
            counter = 0;
            try
            {
                Beep(440, 1000);
            }
            catch (Exception ex)
            {
                
                MessageBox.Show(ex.ToString());
                return;

            }

            //string dir_title = @"C:\Program Files\Touch Demo\TouchSetup";
            //if (Directory.Exists (dir_title))
            //{
                
            //}
            //else
            //{
            //    MessageBox.Show(dir_title + " does not exist");
            //}

            //try
            //{
            //    Environment.CurrentDirectory = (dir_title);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //    return;
            //}




            try
            {

                DLL_TBApiInit(0);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return;
            }
            try
            {
                int result;
                result = DLL_TBApiOpen();
                if (result == 0)
                {
                    MessageBox.Show("Bad Open");
                    return;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return;
            }

            try
            {
                int result;
                result = DLL_TBApiGetRelativeDevice(0);

                //MessageBox.Show("Handle: " + result.ToString());
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return;
            }

            try
            {
                DLL_TBApiMousePortInterfaceEnable(0);                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
            unsafe
            {
                try
                {
                    my_delegate = this.call_function;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return;
                }
            }
           

            try
            {
                int result;
                result = DLL_TBApiRegisterDataCallback(0, 0, 3, my_delegate);
                if (result == 0)
                {
                    MessageBox.Show("Bad Reg.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }




            button1.Enabled = false;
            button2.Enabled = true;
             


        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int result = DLL_TBApiUnregisterDataCallback(my_delegate);
                if (result == 0)
                {
                    MessageBox.Show("Bad UnReg.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            try
            {
                DLL_TBApiMousePortInterfaceEnable(1);                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            try
            {
                DLL_TBApiClose();
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
            try
            {
                DLL_TBApiTerminate();
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }

            try
            {
                Beep(660, 500);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            button2.Enabled = false;
            button1.Enabled = true;

        }

        public unsafe void UpDate_Type_1()
        {
            try
            {
                   if ((my_data_block.tick - last_tick) > 200)
                    {
                        last_tick = my_data_block.tick;

                        textBox2.Text = "Tick: " + my_data_block.tick.ToString() + "\r\n";
                        textBox2.Text += "Rawx: " + my_data_block.rawx.ToString() + "\r\n";
                        textBox2.Text += "Rawy: " + my_data_block.rawy.ToString() + "\r\n";
                        textBox2.Text += "Calx: " + my_data_block.calx.ToString() + "\r\n";
                        textBox2.Text += "Caly: " + my_data_block.caly.ToString() + "\r\n";
                        textBox2.Text += "hDevice: " + my_data_block.hDevice.ToString() + "\r\n";
                        textBox2.Text += "hStylus: " + my_data_block.hStylus.ToString() + "\r\n";
                        textBox2.Text += "Type: " + my_data_block.type.ToString() + "\r\n";
                    }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        public unsafe void UpDate_Type_2()
        {
            try
            {
                    textBox1.Text = "Tick: " + my_event_block.tick.ToString() + "\r\n";
                    textBox1.Text += "Left: " + my_event_block.left.ToString() + "\r\n";
                    textBox1.Text += "Right: " + my_event_block.right.ToString() + "\r\n";
                    textBox1.Text += "hDevice: " + my_event_block.hDevice.ToString() + "\r\n";
                    textBox1.Text += "hStylus: " + my_event_block.hStylus.ToString() + "\r\n";
                    textBox1.Text += "Type: " + my_event_block.type.ToString() + "\r\n";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }




    }
}